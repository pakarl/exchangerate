package se.experis;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

public class Main {
    /**
     * Simon Nilsson
     * Per-Anders Karlsson
     */

    public static final String ANSI_RESET  = "\u001B[0m";
    public static final String ANSI_RED    = "\u001B[31m";
    public static final String ANSI_GREEN  = "\u001B[32m";
    public static final String ANSI_BRIGHT_RED    = "\u001B[91m";
    public static final String ANSI_BRIGHT_GREEN  = "\u001B[92m";


    public static void main(String[] args) throws Exception {

        System.out.println("By: Simon Nilsson & Per-Anders Karlsson\n");
        String url = "https://financialmodelingprep.com/api/v3/forex/";
        System.out.println("Reading from: " + url);

        boolean firstRun = false;
        double newChange = 0;
        double currentChange = getExchange(url);

        //Loop forever, or until you hit Ctrl + c
        while (true) {

            TimeUnit.SECONDS.sleep(5);
            clearScreen();
            System.out.println("By: Simon Nilsson & Per-Anders Karlsson\n");
            System.out.print("1 USD costs ");
            newChange = getExchange(url);
            if (firstRun) {
                if (newChange > currentChange){
                    System.out.print(ANSI_GREEN + newChange);

                }else if (newChange < currentChange){
                    System.out.print(ANSI_RED + newChange);

                }else {
                    System.out.print(newChange);
                }
                currentChange = newChange;
            }else{
                firstRun = true;
                System.out.print(currentChange);
            }
            System.out.print(ANSI_RESET + " SEK");
            System.out.println();
        }
    }

    public static String requestURL(String url) throws Exception {
        // Set URL
        URLConnection connection = new URL(url).openConnection();
        // Set property - avoid 403 (CORS)
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        // Create connection
        connection.connect();
        // Create a buffer of the input
        BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        // Convert the response into a single string
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = buffer.readLine()) != null) {
            stringBuilder.append(line);
        }
        // return the response
        return stringBuilder.toString();
    }
    public static double getExchange(String url) throws Exception {

        JSONObject jo = new JSONObject(requestURL(url));
        JSONArray ja = new JSONArray(jo.get("forexList").toString());
        JSONObject tempObj = new JSONObject();

        //Retrieves the wanted element as object
        for (int i = 0; i < ja.length(); i++) {
            if (ja.get(i).toString().contains("USD/SEK")){
                tempObj = ja.getJSONObject(i);
            }

        }
        //Parses the string of change to double and returns the value
        return Double.parseDouble(tempObj.get("ask").toString());
    }
    public static void clearScreen() {
        System.out.print("\u001B[H\u001B[2J");
        System.out.flush();
    }


}
